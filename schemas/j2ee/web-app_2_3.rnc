# Copyright (c) 2000 Sun Microsystems, Inc.,
# 901 San Antonio Road,
# Palo Alto, California 94303, U.S.A.
# All rights reserved.
# 
# Sun Microsystems, Inc. has intellectual property rights relating to
# technology embodied in the product that is described in this document.
# In particular, and without limitation, these intellectual property
# rights may include one or more of the U.S. patents listed at
# http://www.sun.com/patents and one or more additional patents or
# pending patent applications in the U.S. and in other countries.
# 
# This document and the product to which it pertains are distributed
# under licenses restricting their use, copying, distribution, and
# decompilation.  This document may be reproduced and distributed but may
# not be changed without prior written authorization of Sun and its
# licensors, if any.
# 
# Third-party software, including font technology, is copyrighted and
# licensed from Sun suppliers.
# 
# Sun,  Sun Microsystems,  the Sun logo,  Java,  JavaServer Pages,  Java
# Naming and Directory Interface,  JDBC,  JDK,  JavaMail and  and
# Enterprise JavaBeans are trademarks or registered trademarks of Sun
# Microsystems, Inc. in the U.S. and other countries.
# 
# Federal Acquisitions: Commercial Software - Government Users Subject to
# Standard License Terms and Conditions.
# 
# DOCUMENTATION IS PROVIDED "AS IS" AND ALL EXPRESS OR IMPLIED
# CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED
# WARRANTY OF MERCHANTABILITY, FITNESS FOR FOR A PARTICULAR PURPOSE OR
# NON-INFRINGEMENT, ARE DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH
# DISCLAIMERS ARE HELD TO BE LEGALLY INVALID.
# 
# 
# _________________________________________________________________________
# 
# Copyright (c) 2000 Sun Microsystems, Inc.,
# 901 San Antonio Road,
# Palo Alto, California 94303, E'tats-Unis.
# Tous droits re'serve's.
# 
# Sun Microsystems, Inc. a les droits de proprie'te' intellectuels
# relatants a` la technologie incorpore'e dans le produit qui est de'crit
# dans ce document. En particulier, et sans la limitation, ces droits de
# proprie'te' intellectuels peuvent inclure un ou plus des brevets
# ame'ricains e'nume're's a` http://www.sun.com/patents et un ou les
# brevets plus supple'mentaires ou les applications de brevet en attente
# dans les E'tats-Unis et dans les autres pays.
# 
# Ce produit ou document est prote'ge' par un copyright et distribue'
# avec des licences qui en restreignent l'utilisation, la copie, la
# distribution, et la de'compilation.  Ce documention associe n peut
# e^tre reproduite et distribuer, par quelque moyen que ce soit, sans
# l'autorisation pre'alable et e'crite de Sun et de ses bailleurs de
# licence, le cas e'che'ant.
# 
# Le logiciel de'tenu par des tiers, et qui comprend la technologie
# relative aux polices de caracte`res, est prote'ge' par un copyright et
# licencie' par des fournisseurs de Sun.
# 
# Sun,  Sun Microsystems,  le logo Sun,  Java,  JavaServer Pages,  Java
# Naming and Directory Interface,  JDBC,  JDK,  JavaMail et  and
# Enterprise JavaBeans sont des marques de fabrique ou des marques
# de'pose'es de Sun Microsystems, Inc. aux E'tats-Unis et dans d'autres
# pays.
# 
# LA DOCUMENTATION EST FOURNIE "EN L'E'TAT" ET TOUTES AUTRES CONDITIONS,
# DECLARATIONS ET GARANTIES EXPRESSES OU TACITES SONT FORMELLEMENT
# EXCLUES, DANS LA MESURE AUTORISEE PAR LA LOI APPLICABLE, Y COMPRIS
# NOTAMMENT TOUTE GARANTIE IMPLICITE RELATIVE A LA QUALITE MARCHANDE, A
# L'APTITUDE A UNE UTILISATION PARTICULIERE OU A L'ABSENCE DE
# CONTREFAC,ON.

# This is the XML DTD for the Servlet 2.3 deployment descriptor.
# All Servlet 2.3 deployment descriptors must include a DOCTYPE
# of the following form:
# 
#   <!DOCTYPE web-app PUBLIC
#	"-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
#	"http://java.sun.com/dtd/web-app_2_3.dtd">
#

# The following conventions apply to all J2EE deployment descriptor
# elements unless indicated otherwise.
# 
# - In elements that contain PCDATA, leading and trailing whitespace
#   in the data may be ignored.
# 
# - In elements whose value is an "enumerated type", the value is
#   case sensitive.
# 
# - In elements that specify a pathname to a file within the same
#   JAR file, relative filenames (i.e., those not starting with "/")
#   are considered relative to the root of the JAR file's namespace.
#   Absolute filenames (i.e., those starting with "/") also specify
#   names in the root of the JAR file's namespace.  In general, relative
#   names are preferred.  The exception is .war files where absolute
#   names are preferred for consistency with the servlet API.

# The web-app element is the root of the deployment descriptor for
# a web application.

web-app =
  element web-app {
    attlist.web-app,
    icon?,
    display-name?,
    description?,
    distributable?,
    context-param*,
    filter*,
    filter-mapping*,
    listener*,
    servlet*,
    servlet-mapping*,
    session-config?,
    mime-mapping*,
    welcome-file-list?,
    error-page*,
    taglib*,
    resource-env-ref*,
    resource-ref*,
    security-constraint*,
    login-config?,
    security-role*,
    env-entry*,
    ejb-ref*,
    ejb-local-ref*
  }
# The auth-constraint element indicates the user roles that should
# be permitted access to this resource collection. The role-name
# used here must either correspond to the role-name of one of the
# security-role elements defined for this web application, or be
# the specially reserved role-name "*" that is a compact syntax for
# indicating all roles in the web application. If both "*" and
# rolenames appear, the container interprets this as all roles.
# If no roles are defined, no user is allowed access to the portion of
# the web application described by the containing security-constraint.
# The container matches role names case sensitively when determining
# access.
# 
# 
# Used in: security-constraint
auth-constraint =
  element auth-constraint {
    attlist.auth-constraint, description?, role-name*
  }
# The auth-method element is used to configure the authentication
# mechanism for the web application. As a prerequisite to gaining access to any web resources which are protected by an authorization
# constraint, a user must have authenticated using the configured
# mechanism. Legal values for this element are "BASIC", "DIGEST",
# "FORM", or "CLIENT-CERT".
# 
# Used in: login-config
auth-method = element auth-method { attlist.auth-method, text }
# The context-param element contains the declaration of a web
# application's servlet context initialization parameters.
# 
# Used in: web-app
context-param =
  element context-param {
    attlist.context-param, param-name, param-value, description?
  }
# The description element is used to provide text describing the parent
# element.  The description element should include any information that
# the web application war file producer wants to provide to the consumer of
# the web application war file (i.e., to the Deployer). Typically, the tools
# used by the web application war file consumer will display the description
# when processing the parent element that contains the description.
# 
# Used in: auth-constraint, context-param, ejb-local-ref, ejb-ref,
# env-entry, filter, init-param, resource-env-ref, resource-ref, run-as,
# security-role, security-role-ref, servlet, user-data-constraint,
# web-app, web-resource-collection
description = element description { attlist.description, text }
# The display-name element contains a short name that is intended to be
# displayed by tools.  The display name need not be unique.
# 
# Used in: filter, security-constraint, servlet, web-app
# 
# Example:
# 
# <display-name>Employee Self Service</display-name>
display-name = element display-name { attlist.display-name, text }
# The distributable element, by its presence in a web application
# deployment descriptor, indicates that this web application is
# programmed appropriately to be deployed into a distributed servlet
# container
# 
# Used in: web-app
distributable = element distributable { attlist.distributable, empty }
# The ejb-link element is used in the ejb-ref or ejb-local-ref
# elements to specify that an EJB reference is linked to an
# enterprise bean.
# 
# The name in the ejb-link element is composed of a
# path name specifying the ejb-jar containing the referenced enterprise
# bean with the ejb-name of the target bean appended and separated from
# the path name by "#".  The path name is relative to the war file
# containing the web application that is referencing the enterprise bean.
# This allows multiple enterprise beans with the same ejb-name to be
# uniquely identified.
# 
# Used in: ejb-local-ref, ejb-ref
# 
# Examples:
# 
#	<ejb-link>EmployeeRecord</ejb-link>
# 
#	<ejb-link>../products/product.jar#ProductEJB</ejb-link>
#
ejb-link = element ejb-link { attlist.ejb-link, text }
# The ejb-local-ref element is used for the declaration of a reference to
# an enterprise bean's local home. The declaration consists of:
# 
#	- an optional description
#	- the EJB reference name used in the code of the web application
#	  that's referencing the enterprise bean
#	- the expected type of the referenced enterprise bean
#	- the expected local home and local interfaces of the referenced
#	  enterprise bean
#	- optional ejb-link information, used to specify the referenced
#	  enterprise bean
# 
# Used in: web-app
ejb-local-ref =
  element ejb-local-ref {
    attlist.ejb-local-ref,
    description?,
    ejb-ref-name,
    ejb-ref-type,
    local-home,
    local,
    ejb-link?
  }
# The ejb-ref element is used for the declaration of a reference to
# an enterprise bean's home. The declaration consists of:
# 
#	- an optional description
#	- the EJB reference name used in the code of
#	  the web application that's referencing the enterprise bean
#	- the expected type of the referenced enterprise bean
#	- the expected home and remote interfaces of the referenced
#	  enterprise bean
#	- optional ejb-link information, used to specify the referenced
#	  enterprise bean
# 
# Used in: web-app
ejb-ref =
  element ejb-ref {
    attlist.ejb-ref,
    description?,
    ejb-ref-name,
    ejb-ref-type,
    home,
    remote,
    ejb-link?
  }
# The ejb-ref-name element contains the name of an EJB reference. The
# EJB reference is an entry in the web application's environment and is
# relative to the java:comp/env context.  The name must be unique
# within the web application.
# 
# It is recommended that name is prefixed with "ejb/".
# 
# Used in: ejb-local-ref, ejb-ref
# 
# Example:
# 
# <ejb-ref-name>ejb/Payroll</ejb-ref-name>
ejb-ref-name = element ejb-ref-name { attlist.ejb-ref-name, text }
# The ejb-ref-type element contains the expected type of the
# referenced enterprise bean.
# 
# The ejb-ref-type element must be one of the following:
# 
#	<ejb-ref-type>Entity</ejb-ref-type>
#	<ejb-ref-type>Session</ejb-ref-type>
# 
# Used in: ejb-local-ref, ejb-ref
ejb-ref-type = element ejb-ref-type { attlist.ejb-ref-type, text }
# The env-entry element contains the declaration of a web application's
# environment entry. The declaration consists of an optional
# description, the name of the environment entry, and an optional
# value.  If a value is not specified, one must be supplied
# during deployment.
env-entry =
  element env-entry {
    attlist.env-entry,
    description?,
    env-entry-name,
    env-entry-value?,
    env-entry-type
  }
# The env-entry-name element contains the name of a web applications's
# environment entry.  The name is a JNDI name relative to the
# java:comp/env context.  The name must be unique within a web application.
# 
# Example:
# 
# <env-entry-name>minAmount</env-entry-name>
# 
# Used in: env-entry
env-entry-name = element env-entry-name { attlist.env-entry-name, text }
# The env-entry-type element contains the fully-qualified Java type of
# the environment entry value that is expected by the web application's
# code.
# 
# The following are the legal values of env-entry-type:
# 
#	java.lang.Boolean
#	java.lang.Byte
#	java.lang.Character
#	java.lang.String
#	java.lang.Short
#	java.lang.Integer
#	java.lang.Long
#	java.lang.Float
#	java.lang.Double
# 
# Used in: env-entry
env-entry-type = element env-entry-type { attlist.env-entry-type, text }
# The env-entry-value element contains the value of a web application's
# environment entry. The value must be a String that is valid for the
# constructor of the specified type that takes a single String
# parameter, or for java.lang.Character, a single character.
# 
# Example:
# 
# <env-entry-value>100.00</env-entry-value>
# 
# Used in: env-entry
env-entry-value =
  element env-entry-value { attlist.env-entry-value, text }
# The error-code contains an HTTP error code, ex: 404
# 
# Used in: error-page
error-code = element error-code { attlist.error-code, text }
# The error-page element contains a mapping between an error code
# or exception type to the path of a resource in the web application
# 
# Used in: web-app
error-page =
  element error-page {
    attlist.error-page, (error-code | exception-type), location
  }
# The exception type contains a fully qualified class name of a
# Java exception type.
# 
# Used in: error-page
exception-type = element exception-type { attlist.exception-type, text }
# The extension element contains a string describing an
# extension. example: "txt"
# 
# Used in: mime-mapping
extension = element extension { attlist.extension, text }
# Declares a filter in the web application. The filter is mapped to
# either a servlet or a URL pattern in the filter-mapping element, using
# the filter-name value to reference. Filters can access the
# initialization parameters declared in the deployment descriptor at
# runtime via the FilterConfig interface.
# 
# Used in: web-app
filter =
  element filter {
    attlist.filter,
    icon?,
    filter-name,
    display-name?,
    description?,
    filter-class,
    init-param*
  }
# The fully qualified classname of the filter.
# 
# Used in: filter
filter-class = element filter-class { attlist.filter-class, text }
# Declaration of the filter mappings in this web application. The
# container uses the filter-mapping declarations to decide which filters
# to apply to a request, and in what order. The container matches the
# request URI to a Servlet in the normal way. To determine which filters
# to apply it matches filter-mapping declarations either on servlet-name,
# or on url-pattern for each filter-mapping element, depending on which
# style is used. The order in which filters are invoked is the order in
# which filter-mapping declarations that match a request URI for a
# servlet appear in the list of filter-mapping elements.The filter-name
# value must be the value of the <filter-name> sub-elements of one of the
# <filter> declarations in the deployment descriptor.
# 
# Used in: web-app
filter-mapping =
  element filter-mapping {
    attlist.filter-mapping, filter-name, (url-pattern | servlet-name)
  }
# The logical name of the filter. This name is used to map the filter.
# Each filter name is unique within the web application.
# 
# Used in: filter, filter-mapping
filter-name = element filter-name { attlist.filter-name, text }
# The form-error-page element defines the location in the web app
# where the error page that is displayed when login is not successful
# can be found. The path begins with a leading / and is interpreted
# relative to the root of the WAR.
# 
# Used in: form-login-config
form-error-page =
  element form-error-page { attlist.form-error-page, text }
# The form-login-config element specifies the login and error pages
# that should be used in form based login. If form based authentication
# is not used, these elements are ignored.
# 
# Used in: login-config
form-login-config =
  element form-login-config {
    attlist.form-login-config, form-login-page, form-error-page
  }
# The form-login-page element defines the location in the web app
# where the page that can be used for login can be found. The path
# begins with a leading / and is interpreted relative to the root of the WAR.
# 
# Used in: form-login-config
form-login-page =
  element form-login-page { attlist.form-login-page, text }
# The home element contains the fully-qualified name of the enterprise
# bean's home interface.
# 
# Used in: ejb-ref
# 
# Example:
# 
# <home>com.aardvark.payroll.PayrollHome</home>
home = element home { attlist.home, text }
# The http-method contains an HTTP method (GET | POST |...).
# 
# Used in: web-resource-collection
http-method = element http-method { attlist.http-method, text }
# The icon element contains small-icon and large-icon elements that
# specify the file names for small and a large GIF or JPEG icon images
# used to represent the parent element in a GUI tool.
# 
# Used in: filter, servlet, web-app
icon = element icon { attlist.icon, small-icon?, large-icon? }
# The init-param element contains a name/value pair as an
# initialization param of the servlet
# 
# Used in: filter, servlet
init-param =
  element init-param {
    attlist.init-param, param-name, param-value, description?
  }
# The jsp-file element contains the full path to a JSP file within
# the web application beginning with a `/'.
# 
# Used in: servlet
jsp-file = element jsp-file { attlist.jsp-file, text }
# The large-icon element contains the name of a file
# containing a large (32 x 32) icon image. The file
# name is a relative path within the web application's
# war file.
# 
# The image may be either in the JPEG or GIF format.
# The icon can be used by tools.
# 
# Used in: icon
# 
# Example:
# 
# <large-icon>employee-service-icon32x32.jpg</large-icon>
large-icon = element large-icon { attlist.large-icon, text }
# The listener element indicates the deployment properties for a web
# application listener bean.
# 
# Used in: web-app
listener = element listener { attlist.listener, listener-class }
# The listener-class element declares a class in the application must be
# registered as a web application listener bean. The value is the fully qualified classname of the listener class.
# 
# 
# Used in: listener
listener-class = element listener-class { attlist.listener-class, text }
# The load-on-startup element indicates that this servlet should be
# loaded (instantiated and have its init() called) on the startup
# of the web application. The optional contents of
# these element must be an integer indicating the order in which
# the servlet should be loaded. If the value is a negative integer,
# or the element is not present, the container is free to load the
# servlet whenever it chooses. If the value is a positive integer
# or 0, the container must load and initialize the servlet as the
# application is deployed. The container must guarantee that
# servlets marked with lower integers are loaded before servlets
# marked with higher integers. The container may choose the order
# of loading of servlets with the same load-on-start-up value.
# 
# Used in: servlet
load-on-startup =
  element load-on-startup { attlist.load-on-startup, text }
# 
# The local element contains the fully-qualified name of the
# enterprise bean's local interface.
# 
# Used in: ejb-local-ref
#
local = element local { attlist.local, text }
# 
# The local-home element contains the fully-qualified name of the
# enterprise bean's local home interface.
# 
# Used in: ejb-local-ref
local-home = element local-home { attlist.local-home, text }
# The location element contains the location of the resource in the web
# application relative to the root of the web application. The value of
# the location must have a leading `/'.
# 
# Used in: error-page
location = element location { attlist.location, text }
# The login-config element is used to configure the authentication
# method that should be used, the realm name that should be used for
# this application, and the attributes that are needed by the form login
# mechanism.
# 
# Used in: web-app
login-config =
  element login-config {
    attlist.login-config, auth-method?, realm-name?, form-login-config?
  }
# The mime-mapping element defines a mapping between an extension
# and a mime type.
# 
# Used in: web-app
mime-mapping =
  element mime-mapping { attlist.mime-mapping, extension, mime-type }
# The mime-type element contains a defined mime type. example:
# "text/plain"
# 
# Used in: mime-mapping
mime-type = element mime-type { attlist.mime-type, text }
# The param-name element contains the name of a parameter. Each parameter
# name must be unique in the web application.
# 
# 
# Used in: context-param, init-param
param-name = element param-name { attlist.param-name, text }
# The param-value element contains the value of a parameter.
# 
# Used in: context-param, init-param
param-value = element param-value { attlist.param-value, text }
# The realm name element specifies the realm name to use in HTTP
# Basic authorization.
# 
# Used in: login-config
realm-name = element realm-name { attlist.realm-name, text }
# The remote element contains the fully-qualified name of the enterprise
# bean's remote interface.
# 
# Used in: ejb-ref
# 
# Example:
# 
# <remote>com.wombat.empl.EmployeeService</remote>
remote = element remote { attlist.remote, text }
# The res-auth element specifies whether the web application code signs
# on programmatically to the resource manager, or whether the Container
# will sign on to the resource manager on behalf of the web application. In the
# latter case, the Container uses information that is supplied by the
# Deployer.
# 
# The value of this element must be one of the two following:
# 
#	<res-auth>Application</res-auth>
#	<res-auth>Container</res-auth>
# 
# Used in: resource-ref
res-auth = element res-auth { attlist.res-auth, text }
# The res-ref-name element specifies the name of a resource manager
# connection factory reference.  The name is a JNDI name relative to the
# java:comp/env context.  The name must be unique within a web application.
# 
# Used in: resource-ref
res-ref-name = element res-ref-name { attlist.res-ref-name, text }
# The res-sharing-scope element specifies whether connections obtained
# through the given resource manager connection factory reference can be
# shared. The value of this element, if specified, must be one of the
# two following:
# 
#	<res-sharing-scope>Shareable</res-sharing-scope>
#	<res-sharing-scope>Unshareable</res-sharing-scope>
# 
# The default value is Shareable.
# 
# Used in: resource-ref
res-sharing-scope =
  element res-sharing-scope { attlist.res-sharing-scope, text }
# The res-type element specifies the type of the data source. The type
# is specified by the fully qualified Java language class or interface
# expected to be implemented by the data source.
# 
# Used in: resource-ref
res-type = element res-type { attlist.res-type, text }
# The resource-env-ref element contains a declaration of a web application's
# reference to an administered object associated with a resource
# in the web application's environment.  It consists of an optional
# description, the resource environment reference name, and an
# indication of the resource environment reference type expected by
# the web application code.
# 
# Used in: web-app
# 
# Example:
# 
# <resource-env-ref>
#     <resource-env-ref-name>jms/StockQueue</resource-env-ref-name>
#     <resource-env-ref-type>javax.jms.Queue</resource-env-ref-type>
# </resource-env-ref>
resource-env-ref =
  element resource-env-ref {
    attlist.resource-env-ref,
    description?,
    resource-env-ref-name,
    resource-env-ref-type
  }
# The resource-env-ref-name element specifies the name of a resource
# environment reference; its value is the environment entry name used in
# the web application code.  The name is a JNDI name relative to the
# java:comp/env context and must be unique within a web application.
# 
# Used in: resource-env-ref
resource-env-ref-name =
  element resource-env-ref-name { attlist.resource-env-ref-name, text }
# The resource-env-ref-type element specifies the type of a resource
# environment reference.  It is the fully qualified name of a Java
# language class or interface.
# 
# Used in: resource-env-ref
resource-env-ref-type =
  element resource-env-ref-type { attlist.resource-env-ref-type, text }
# The resource-ref element contains a declaration of a web application's
# reference to an external resource. It consists of an optional
# description, the resource manager connection factory reference name,
# the indication of the resource manager connection factory type
# expected by the web application code, the type of authentication
# (Application or Container), and an optional specification of the
# shareability of connections obtained from the resource (Shareable or
# Unshareable).
# 
# Used in: web-app
# 
# Example:
# 
#     <resource-ref>
#	<res-ref-name>jdbc/EmployeeAppDB</res-ref-name>
#	<res-type>javax.sql.DataSource</res-type>
#	<res-auth>Container</res-auth>
#	<res-sharing-scope>Shareable</res-sharing-scope>
#     </resource-ref>
resource-ref =
  element resource-ref {
    attlist.resource-ref,
    description?,
    res-ref-name,
    res-type,
    res-auth,
    res-sharing-scope?
  }
# The role-link element is a reference to a defined security role. The
# role-link element must contain the name of one of the security roles
# defined in the security-role elements.
# 
# Used in: security-role-ref
role-link = element role-link { attlist.role-link, text }
# The role-name element contains the name of a security role.
# 
# The name must conform to the lexical rules for an NMTOKEN.
# 
# Used in: auth-constraint, run-as, security-role, security-role-ref
role-name = element role-name { attlist.role-name, text }
# The run-as element specifies the run-as identity to be used for the
# execution of the web application. It contains an optional description, and
# the name of a security role.
# 
# Used in: servlet
run-as = element run-as { attlist.run-as, description?, role-name }
# The security-constraint element is used to associate security
# constraints with one or more web resource collections
# 
# Used in: web-app
security-constraint =
  element security-constraint {
    attlist.security-constraint,
    display-name?,
    web-resource-collection+,
    auth-constraint?,
    user-data-constraint?
  }
# The security-role element contains the definition of a security
# role. The definition consists of an optional description of the
# security role, and the security role name.
# 
# Used in: web-app
# 
# Example:
# 
#     <security-role>
#	<description>
#	    This role includes all employees who are authorized
#	    to access the employee service application.
#	</description>
#	<role-name>employee</role-name>
#     </security-role>
security-role =
  element security-role {
    attlist.security-role, description?, role-name
  }
# The security-role-ref element contains the declaration of a security
# role reference in the web application's code. The declaration consists
# of an optional description, the security role name used in the code,
# and an optional link to a security role. If the security role is not
# specified, the Deployer must choose an appropriate security role.
# 
# The value of the role-name element must be the String used as the
# parameter to the EJBContext.isCallerInRole(String roleName) method
# or the HttpServletRequest.isUserInRole(String role) method.
# 
# Used in: servlet
#
security-role-ref =
  element security-role-ref {
    attlist.security-role-ref, description?, role-name, role-link?
  }
# The servlet element contains the declarative data of a
# servlet. If a jsp-file is specified and the load-on-startup element is
# present, then the JSP should be precompiled and loaded.
# 
# Used in: web-app
servlet =
  element servlet {
    attlist.servlet,
    icon?,
    servlet-name,
    display-name?,
    description?,
    (servlet-class | jsp-file),
    init-param*,
    load-on-startup?,
    run-as?,
    security-role-ref*
  }
# The servlet-class element contains the fully qualified class name
# of the servlet.
# 
# Used in: servlet
servlet-class = element servlet-class { attlist.servlet-class, text }
# The servlet-mapping element defines a mapping between a servlet
# and a url pattern
# 
# Used in: web-app
servlet-mapping =
  element servlet-mapping {
    attlist.servlet-mapping, servlet-name, url-pattern
  }
# The servlet-name element contains the canonical name of the
# servlet. Each servlet name is unique within the web application.
# 
# Used in: filter-mapping, servlet, servlet-mapping
servlet-name = element servlet-name { attlist.servlet-name, text }
# The session-config element defines the session parameters for
# this web application.
# 
# Used in: web-app
session-config =
  element session-config { attlist.session-config, session-timeout? }
# The session-timeout element defines the default session timeout
# interval for all sessions created in this web application. The
# specified timeout must be expressed in a whole number of minutes.
# If the timeout is 0 or less, the container ensures the default
# behaviour of sessions is never to time out.
# 
# Used in: session-config
session-timeout =
  element session-timeout { attlist.session-timeout, text }
# The small-icon element contains the name of a file
# containing a small (16 x 16) icon image. The file
# name is a relative path within the web application's
# war file.
# 
# The image may be either in the JPEG or GIF format.
# The icon can be used by tools.
# 
# Used in: icon
# 
# Example:
# 
# <small-icon>employee-service-icon16x16.jpg</small-icon>
small-icon = element small-icon { attlist.small-icon, text }
# The taglib element is used to describe a JSP tag library.
# 
# Used in: web-app
taglib = element taglib { attlist.taglib, taglib-uri, taglib-location }
# the taglib-location element contains the location (as a resource
# relative to the root of the web application) where to find the Tag
# Libary Description file for the tag library.
# 
# Used in: taglib
taglib-location =
  element taglib-location { attlist.taglib-location, text }
# The taglib-uri element describes a URI, relative to the location
# of the web.xml document, identifying a Tag Library used in the Web
# Application.
# 
# Used in: taglib
taglib-uri = element taglib-uri { attlist.taglib-uri, text }
# The transport-guarantee element specifies that the communication
# between client and server should be NONE, INTEGRAL, or
# CONFIDENTIAL. NONE means that the application does not require any
# transport guarantees. A value of INTEGRAL means that the application
# requires that the data sent between the client and server be sent in
# such a way that it can't be changed in transit. CONFIDENTIAL means
# that the application requires that the data be transmitted in a
# fashion that prevents other entities from observing the contents of
# the transmission. In most cases, the presence of the INTEGRAL or
# CONFIDENTIAL flag will indicate that the use of SSL is required.
# 
# Used in: user-data-constraint
transport-guarantee =
  element transport-guarantee { attlist.transport-guarantee, text }
# The url-pattern element contains the url pattern of the mapping. Must
# follow the rules specified in Section 11.2 of the Servlet API
# Specification.
# 
# Used in: filter-mapping, servlet-mapping, web-resource-collection
url-pattern = element url-pattern { attlist.url-pattern, text }
# The user-data-constraint element is used to indicate how data
# communicated between the client and container should be protected.
# 
# Used in: security-constraint
user-data-constraint =
  element user-data-constraint {
    attlist.user-data-constraint, description?, transport-guarantee
  }
# The web-resource-collection element is used to identify a subset
# of the resources and HTTP methods on those resources within a web
# application to which a security constraint applies. If no HTTP methods
# are specified, then the security constraint applies to all HTTP
# methods.
# 
# Used in: security-constraint
web-resource-collection =
  element web-resource-collection {
    attlist.web-resource-collection,
    web-resource-name,
    description?,
    url-pattern*,
    http-method*
  }
# The web-resource-name contains the name of this web resource
# collection.
# 
# Used in: web-resource-collection
web-resource-name =
  element web-resource-name { attlist.web-resource-name, text }
# The welcome-file element contains file name to use as a default
# welcome file, such as index.html
# 
# Used in: welcome-file-list
welcome-file = element welcome-file { attlist.welcome-file, text }
# The welcome-file-list contains an ordered list of welcome files
# elements.
# 
# Used in: web-app
welcome-file-list =
  element welcome-file-list { attlist.welcome-file-list, welcome-file+ }
# The ID mechanism is to allow tools that produce additional deployment
# information (i.e., information beyond the standard deployment
# descriptor information) to store the non-standard information in a
# separate file, and easily refer from these tool-specific files to the
# information in the standard deployment descriptor.
# 
# Tools are not allowed to add the non-standard information into the
# standard deployment descriptor.
attlist.auth-constraint &= attribute id { xsd:ID }?
attlist.auth-method &= attribute id { xsd:ID }?
attlist.context-param &= attribute id { xsd:ID }?
attlist.description &= attribute id { xsd:ID }?
attlist.display-name &= attribute id { xsd:ID }?
attlist.distributable &= attribute id { xsd:ID }?
attlist.ejb-link &= attribute id { xsd:ID }?
attlist.ejb-local-ref &= attribute id { xsd:ID }?
attlist.ejb-ref &= attribute id { xsd:ID }?
attlist.ejb-ref-name &= attribute id { xsd:ID }?
attlist.ejb-ref-type &= attribute id { xsd:ID }?
attlist.env-entry &= attribute id { xsd:ID }?
attlist.env-entry-name &= attribute id { xsd:ID }?
attlist.env-entry-type &= attribute id { xsd:ID }?
attlist.env-entry-value &= attribute id { xsd:ID }?
attlist.error-code &= attribute id { xsd:ID }?
attlist.error-page &= attribute id { xsd:ID }?
attlist.exception-type &= attribute id { xsd:ID }?
attlist.extension &= attribute id { xsd:ID }?
attlist.filter &= attribute id { xsd:ID }?
attlist.filter-class &= attribute id { xsd:ID }?
attlist.filter-mapping &= attribute id { xsd:ID }?
attlist.filter-name &= attribute id { xsd:ID }?
attlist.form-error-page &= attribute id { xsd:ID }?
attlist.form-login-config &= attribute id { xsd:ID }?
attlist.form-login-page &= attribute id { xsd:ID }?
attlist.home &= attribute id { xsd:ID }?
attlist.http-method &= attribute id { xsd:ID }?
attlist.icon &= attribute id { xsd:ID }?
attlist.init-param &= attribute id { xsd:ID }?
attlist.jsp-file &= attribute id { xsd:ID }?
attlist.large-icon &= attribute id { xsd:ID }?
attlist.listener &= attribute id { xsd:ID }?
attlist.listener-class &= attribute id { xsd:ID }?
attlist.load-on-startup &= attribute id { xsd:ID }?
attlist.local &= attribute id { xsd:ID }?
attlist.local-home &= attribute id { xsd:ID }?
attlist.location &= attribute id { xsd:ID }?
attlist.login-config &= attribute id { xsd:ID }?
attlist.mime-mapping &= attribute id { xsd:ID }?
attlist.mime-type &= attribute id { xsd:ID }?
attlist.param-name &= attribute id { xsd:ID }?
attlist.param-value &= attribute id { xsd:ID }?
attlist.realm-name &= attribute id { xsd:ID }?
attlist.remote &= attribute id { xsd:ID }?
attlist.res-auth &= attribute id { xsd:ID }?
attlist.res-ref-name &= attribute id { xsd:ID }?
attlist.res-sharing-scope &= attribute id { xsd:ID }?
attlist.res-type &= attribute id { xsd:ID }?
attlist.resource-env-ref &= attribute id { xsd:ID }?
attlist.resource-env-ref-name &= attribute id { xsd:ID }?
attlist.resource-env-ref-type &= attribute id { xsd:ID }?
attlist.resource-ref &= attribute id { xsd:ID }?
attlist.role-link &= attribute id { xsd:ID }?
attlist.role-name &= attribute id { xsd:ID }?
attlist.run-as &= attribute id { xsd:ID }?
attlist.security-constraint &= attribute id { xsd:ID }?
attlist.security-role &= attribute id { xsd:ID }?
attlist.security-role-ref &= attribute id { xsd:ID }?
attlist.servlet &= attribute id { xsd:ID }?
attlist.servlet-class &= attribute id { xsd:ID }?
attlist.servlet-mapping &= attribute id { xsd:ID }?
attlist.servlet-name &= attribute id { xsd:ID }?
attlist.session-config &= attribute id { xsd:ID }?
attlist.session-timeout &= attribute id { xsd:ID }?
attlist.small-icon &= attribute id { xsd:ID }?
attlist.taglib &= attribute id { xsd:ID }?
attlist.taglib-location &= attribute id { xsd:ID }?
attlist.taglib-uri &= attribute id { xsd:ID }?
attlist.transport-guarantee &= attribute id { xsd:ID }?
attlist.url-pattern &= attribute id { xsd:ID }?
attlist.user-data-constraint &= attribute id { xsd:ID }?
attlist.web-app &= attribute id { xsd:ID }?
attlist.web-resource-collection &= attribute id { xsd:ID }?
attlist.web-resource-name &= attribute id { xsd:ID }?
attlist.welcome-file &= attribute id { xsd:ID }?
attlist.welcome-file-list &= attribute id { xsd:ID }?
start = web-app
