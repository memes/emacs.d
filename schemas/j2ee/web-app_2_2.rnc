# Copyright 1999 Sun Microsystems, Inc. 901 San Antonio Road,
# Palo Alto, CA  94303, U.S.A.  All rights reserved.
# 
# This product or document is protected by copyright and distributed
# under licenses restricting its use, copying, distribution, and
# decompilation.  No part of this product or documentation may be
# reproduced in any form by any means without prior written authorization
# of Sun and its licensors, if any.  
# 
# Third party software, including font technology, is copyrighted and 
# licensed from Sun suppliers. 
# 
# Sun, Sun Microsystems, the Sun Logo, Solaris, Java, JavaServer Pages, Java 
# Naming and Directory Interface, JDBC, JDK, JavaMail and Enterprise JavaBeans, 
# are trademarks or registered trademarks of Sun Microsystems, Inc in the U.S. 
# and other countries.
# 
# All SPARC trademarks are used under license and are trademarks
# or registered trademarks of SPARC International, Inc.
# in the U.S. and other countries. Products bearing SPARC
# trademarks are based upon an architecture developed by Sun Microsystems, Inc. 
# 
# PostScript is a registered trademark of Adobe Systems, Inc. 
# 
# 
# Federal Acquisitions: Commercial Software - Government Users Subject to 
# Standard License Terms and Conditions.
# 
# 
# 
# DOCUMENTATION IS PROVIDED "AS IS" AND ALL EXPRESS OR IMPLIED
# CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
# IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE OR NON-INFRINGEMENT, ARE DISCLAIMED, EXCEPT
# TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD TO BE LEGALLY
# INVALID.
# 
# _________________________________________________________________________
# Copyright 1999 Sun Microsystems, Inc., 
# 901 San Antonio Road, Palo Alto, CA  94303, Etats-Unis. 
# Tous droits re'serve's.
# 
# 
# Ce produit ou document est prote'ge' par un copyright et distribue' avec 
# des licences qui en restreignent l'utilisation, la copie, la distribution,
# et la de'compilation.  Aucune partie de ce produit ou de sa documentation
# associe'e ne peut e^tre reproduite sous aucune forme, par quelque moyen 
# que ce soit, sans l'autorisation pre'alable et e'crite de Sun et de ses 
# bailleurs de licence, s'il y en a.  
# 
# Le logiciel de'tenu par des tiers, et qui comprend la technologie 
# relative aux polices de caracte`res, est prote'ge' par un copyright 
# et licencie' par des fournisseurs de Sun.
# 
# Sun, Sun Microsystems, le logo Sun, Solaris, Java, JavaServer Pages, Java 
# Naming and Directory Interface, JDBC, JDK, JavaMail, et Enterprise JavaBeans,  
# sont des marques de fabrique ou des marques de'pose'es de Sun 
# Microsystems, Inc. aux Etats-Unis et dans d'autres pays.
# 
# Toutes les marques SPARC sont utilise'es sous licence et sont
# des marques de fabrique ou des marques de'pose'es de SPARC
# International, Inc. aux Etats-Unis et  dans
# d'autres pays. Les produits portant les marques SPARC sont
# base's sur une architecture de'veloppe'e par Sun Microsystems, Inc.  
# 
# Postcript est une marque enregistre'e d'Adobe Systems Inc. 
# 
# LA DOCUMENTATION EST FOURNIE "EN L'ETAT" ET TOUTES AUTRES CONDITIONS,
# DECLARATIONS ET GARANTIES EXPRESSES OU TACITES SONT FORMELLEMENT EXCLUES,
# DANS LA MESURE AUTORISEE PAR LA LOI APPLICABLE, Y COMPRIS NOTAMMENT
# TOUTE GARANTIE IMPLICITE RELATIVE A LA QUALITE MARCHANDE, A L'APTITUDE
# A UNE UTILISATION PARTICULIERE OU A L'ABSENCE DE CONTREFACON.

# The web-app element is the root of the deployment descriptor for
# a web application

web-app =
  element web-app {
    attlist.web-app,
    icon?,
    display-name?,
    description?,
    distributable?,
    context-param*,
    servlet*,
    servlet-mapping*,
    session-config?,
    mime-mapping*,
    welcome-file-list?,
    error-page*,
    taglib*,
    resource-ref*,
    security-constraint*,
    login-config?,
    security-role*,
    env-entry*,
    ejb-ref*
  }
# The icon element contains a small-icon and a large-icon element
# which specify the location within the web application for a small and
# large image used to represent the web application in a GUI tool. At a
# minimum, tools must accept GIF and JPEG format images.
icon = element icon { attlist.icon, small-icon?, large-icon? }
# The small-icon element contains the location within the web
# application of a file containing a small (16x16 pixel) icon image.
small-icon = element small-icon { attlist.small-icon, text }
# The large-icon element contains the location within the web
# application of a file containing a large (32x32 pixel) icon image.
large-icon = element large-icon { attlist.large-icon, text }
# The display-name element contains a short name that is intended
# to be displayed by GUI tools
display-name = element display-name { attlist.display-name, text }
# The description element is used to provide descriptive text about
# the parent element.
description = element description { attlist.description, text }
# The distributable element, by its presence in a web application
# deployment descriptor, indicates that this web application is
# programmed appropriately to be deployed into a distributed servlet
# container
distributable = element distributable { attlist.distributable, empty }
# The context-param element contains the declaration of a web
# application's servlet context initialization parameters.
context-param =
  element context-param {
    attlist.context-param, param-name, param-value, description?
  }
# The param-name element contains the name of a parameter.
param-name = element param-name { attlist.param-name, text }
# The param-value element contains the value of a parameter.
param-value = element param-value { attlist.param-value, text }
# The servlet element contains the declarative data of a
# servlet. If a jsp-file is specified and the load-on-startup element is
# present, then the JSP should be precompiled and loaded.
servlet =
  element servlet {
    attlist.servlet,
    icon?,
    servlet-name,
    display-name?,
    description?,
    (servlet-class | jsp-file),
    init-param*,
    load-on-startup?,
    security-role-ref*
  }
# The servlet-name element contains the canonical name of the
# servlet.
servlet-name = element servlet-name { attlist.servlet-name, text }
# The servlet-class element contains the fully qualified class name
# of the servlet.
servlet-class = element servlet-class { attlist.servlet-class, text }
# The jsp-file element contains the full path to a JSP file within
# the web application.
jsp-file = element jsp-file { attlist.jsp-file, text }
# The init-param element contains a name/value pair as an
# initialization param of the servlet
init-param =
  element init-param {
    attlist.init-param, param-name, param-value, description?
  }
# The load-on-startup element indicates that this servlet should be
# loaded on the startup of the web application. The optional contents of
# these element must be a positive integer indicating the order in which
# the servlet should be loaded. Lower integers are loaded before higher
# integers. If no value is specified, or if the value specified is not a
# positive integer, the container is free to load it at any time in the
# startup sequence.
load-on-startup =
  element load-on-startup { attlist.load-on-startup, text }
# The servlet-mapping element defines a mapping between a servlet
# and a url pattern
servlet-mapping =
  element servlet-mapping {
    attlist.servlet-mapping, servlet-name, url-pattern
  }
# The url-pattern element contains the url pattern of the
# mapping. Must follow the rules specified in Section 10 of the Servlet
# API Specification.
url-pattern = element url-pattern { attlist.url-pattern, text }
# The session-config element defines the session parameters for
# this web application.
session-config =
  element session-config { attlist.session-config, session-timeout? }
# The session-timeout element defines the default session timeout
# interval for all sessions created in this web application. The
# specified timeout must be expressed in a whole number of minutes.
session-timeout =
  element session-timeout { attlist.session-timeout, text }
# The mime-mapping element defines a mapping between an extension
# and a mime type.
mime-mapping =
  element mime-mapping { attlist.mime-mapping, extension, mime-type }
# The extension element contains a string describing an
# extension. example: "txt"
extension = element extension { attlist.extension, text }
# The mime-type element contains a defined mime type. example:
# "text/plain"
mime-type = element mime-type { attlist.mime-type, text }
# The welcome-file-list contains an ordered list of welcome files
# elements.
welcome-file-list =
  element welcome-file-list { attlist.welcome-file-list, welcome-file+ }
# The welcome-file element contains file name to use as a default
# welcome file, such as index.html
welcome-file = element welcome-file { attlist.welcome-file, text }
# The taglib element is used to describe a JSP tag library.
taglib = element taglib { attlist.taglib, taglib-uri, taglib-location }
# The taglib-uri element describes a URI, relative to the location
# of the web.xml document, identifying a Tag Library used in the Web
# Application.
taglib-uri = element taglib-uri { attlist.taglib-uri, text }
# the taglib-location element contains the location (as a resource
# relative to the root of the web application) where to find the Tag
# Libary Description file for the tag library.
taglib-location =
  element taglib-location { attlist.taglib-location, text }
# The error-page element contains a mapping between an error code
# or exception type to the path of a resource in the web application
error-page =
  element error-page {
    attlist.error-page, (error-code | exception-type), location
  }
# The error-code contains an HTTP error code, ex: 404
error-code = element error-code { attlist.error-code, text }
# The exception type contains a fully qualified class name of a
# Java exception type.
exception-type = element exception-type { attlist.exception-type, text }
# The location element contains the location of the resource in the
# web application
location = element location { attlist.location, text }
# The resource-ref element contains a declaration of a Web
# Application's reference to an external resource.
resource-ref =
  element resource-ref {
    attlist.resource-ref, description?, res-ref-name, res-type, res-auth
  }
# The res-ref-name element specifies the name of the resource
# factory reference name.
res-ref-name = element res-ref-name { attlist.res-ref-name, text }
# The res-type element specifies the (Java class) type of the data
# source.
res-type = element res-type { attlist.res-type, text }
# The res-auth element indicates whether the application component
# code performs resource signon programmatically or whether the
# container signs onto the resource based on the principle mapping
# information supplied by the deployer. Must be CONTAINER or SERVLET
res-auth = element res-auth { attlist.res-auth, text }
# The security-constraint element is used to associate security
# constraints with one or more web resource collections
security-constraint =
  element security-constraint {
    attlist.security-constraint,
    web-resource-collection+,
    auth-constraint?,
    user-data-constraint?
  }
# The web-resource-collection element is used to identify a subset
# of the resources and HTTP methods on those resources within a web
# application to which a security constraint applies. If no HTTP methods
# are specified, then the security constraint applies to all HTTP
# methods.
web-resource-collection =
  element web-resource-collection {
    attlist.web-resource-collection,
    web-resource-name,
    description?,
    url-pattern*,
    http-method*
  }
# The web-resource-name contains the name of this web resource
# collection
web-resource-name =
  element web-resource-name { attlist.web-resource-name, text }
# The http-method contains an HTTP method (GET | POST |...)
http-method = element http-method { attlist.http-method, text }
# The user-data-constraint element is used to indicate how data
# communicated between the client and container should be protected
user-data-constraint =
  element user-data-constraint {
    attlist.user-data-constraint, description?, transport-guarantee
  }
# The transport-guarantee element specifies that the communication
# between client and server should be NONE, INTEGRAL, or
# CONFIDENTIAL. NONE means that the application does not require any
# transport guarantees. A value of INTEGRAL means that the application
# requires that the data sent between the client and server be sent in
# such a way that it can't be changed in transit. CONFIDENTIAL means
# that the application requires that the data be transmitted in a
# fashion that prevents other entities from observing the contents of
# the transmission. In most cases, the presence of the INTEGRAL or
# CONFIDENTIAL flag will indicate that the use of SSL is required.
transport-guarantee =
  element transport-guarantee { attlist.transport-guarantee, text }
# The auth-constraint element indicates the user roles that should
# be permitted access to this resource collection. The role used here
# must appear in a security-role-ref element.
auth-constraint =
  element auth-constraint {
    attlist.auth-constraint, description?, role-name*
  }
# The role-name element contains the name of a security role.
role-name = element role-name { attlist.role-name, text }
# The login-config element is used to configure the authentication
# method that should be used, the realm name that should be used for
# this application, and the attributes that are needed by the form login
# mechanism.
login-config =
  element login-config {
    attlist.login-config, auth-method?, realm-name?, form-login-config?
  }
# The realm name element specifies the realm name to use in HTTP
# Basic authorization
realm-name = element realm-name { attlist.realm-name, text }
# The form-login-config element specifies the login and error pages
# that should be used in form based login. If form based authentication
# is not used, these elements are ignored.
form-login-config =
  element form-login-config {
    attlist.form-login-config, form-login-page, form-error-page
  }
# The form-login-page element defines the location in the web app
# where the page that can be used for login can be found
form-login-page =
  element form-login-page { attlist.form-login-page, text }
# The form-error-page element defines the location in the web app
# where the error page that is displayed when login is not successful
# can be found
form-error-page =
  element form-error-page { attlist.form-error-page, text }
# The auth-method element is used to configure the authentication
# mechanism for the web application. As a prerequisite to gaining access
# to any web resources which are protected by an authorization
# constraint, a user must have authenticated using the configured
# mechanism. Legal values for this element are "BASIC", "DIGEST",
# "FORM", or "CLIENT-CERT".
auth-method = element auth-method { attlist.auth-method, text }
# The security-role element contains the declaration of a security
# role which is used in the security-constraints placed on the web
# application.
security-role =
  element security-role {
    attlist.security-role, description?, role-name
  }
# The role-name element contains the name of a role. This element
# must contain a non-empty string.
security-role-ref =
  element security-role-ref {
    attlist.security-role-ref, description?, role-name, role-link
  }
# The role-link element is used to link a security role reference
# to a defined security role. The role-link element must contain the
# name of one of the security roles defined in the security-role
# elements.
role-link = element role-link { attlist.role-link, text }
# The env-entry element contains the declaration of an
# application's environment entry. This element is required to be
# honored on in J2EE compliant servlet containers.
env-entry =
  element env-entry {
    attlist.env-entry,
    description?,
    env-entry-name,
    env-entry-value?,
    env-entry-type
  }
# The env-entry-name contains the name of an application's
# environment entry
env-entry-name = element env-entry-name { attlist.env-entry-name, text }
# The env-entry-value element contains the value of an
# application's environment entry
env-entry-value =
  element env-entry-value { attlist.env-entry-value, text }
# The env-entry-type element contains the fully qualified Java type
# of the environment entry value that is expected by the application
# code. The following are the legal values of env-entry-type:
# java.lang.Boolean, java.lang.String, java.lang.Integer,
# java.lang.Double, java.lang.Float.
env-entry-type = element env-entry-type { attlist.env-entry-type, text }
# The ejb-ref element is used to declare a reference to an
# enterprise bean. 
ejb-ref =
  element ejb-ref {
    attlist.ejb-ref,
    description?,
    ejb-ref-name,
    ejb-ref-type,
    home,
    remote,
    ejb-link?
  }
# The ejb-ref-name element contains the name of an EJB
# reference. This is the JNDI name that the servlet code uses to get a
# reference to the enterprise bean.
ejb-ref-name = element ejb-ref-name { attlist.ejb-ref-name, text }
# The ejb-ref-type element contains the expected java class type of
# the referenced EJB.
ejb-ref-type = element ejb-ref-type { attlist.ejb-ref-type, text }
# The ejb-home element contains the fully qualified name of the
# EJB's home interface
home = element home { attlist.home, text }
# The ejb-remote element contains the fully qualified name of the
# EJB's remote interface
remote = element remote { attlist.remote, text }
# The ejb-link element is used in the ejb-ref element to specify
# that an EJB reference is linked to an EJB in an encompassing Java2
# Enterprise Edition (J2EE) application package. The value of the
# ejb-link element must be the ejb-name of and EJB in the J2EE
# application package.
ejb-link = element ejb-link { attlist.ejb-link, text }
# The ID mechanism is to allow tools to easily make tool-specific
# references to the elements of the deployment descriptor. This allows
# tools that produce additional deployment information (i.e information
# beyond the standard deployment descriptor information) to store the
# non-standard information in a separate file, and easily refer from
# these tools-specific files to the information in the standard web-app
# deployment descriptor.
attlist.web-app &= attribute id { xsd:ID }?
attlist.icon &= attribute id { xsd:ID }?
attlist.small-icon &= attribute id { xsd:ID }?
attlist.large-icon &= attribute id { xsd:ID }?
attlist.display-name &= attribute id { xsd:ID }?
attlist.description &= attribute id { xsd:ID }?
attlist.distributable &= attribute id { xsd:ID }?
attlist.context-param &= attribute id { xsd:ID }?
attlist.param-name &= attribute id { xsd:ID }?
attlist.param-value &= attribute id { xsd:ID }?
attlist.servlet &= attribute id { xsd:ID }?
attlist.servlet-name &= attribute id { xsd:ID }?
attlist.servlet-class &= attribute id { xsd:ID }?
attlist.jsp-file &= attribute id { xsd:ID }?
attlist.init-param &= attribute id { xsd:ID }?
attlist.load-on-startup &= attribute id { xsd:ID }?
attlist.servlet-mapping &= attribute id { xsd:ID }?
attlist.url-pattern &= attribute id { xsd:ID }?
attlist.session-config &= attribute id { xsd:ID }?
attlist.session-timeout &= attribute id { xsd:ID }?
attlist.mime-mapping &= attribute id { xsd:ID }?
attlist.extension &= attribute id { xsd:ID }?
attlist.mime-type &= attribute id { xsd:ID }?
attlist.welcome-file-list &= attribute id { xsd:ID }?
attlist.welcome-file &= attribute id { xsd:ID }?
attlist.taglib &= attribute id { xsd:ID }?
attlist.taglib-uri &= attribute id { xsd:ID }?
attlist.taglib-location &= attribute id { xsd:ID }?
attlist.error-page &= attribute id { xsd:ID }?
attlist.error-code &= attribute id { xsd:ID }?
attlist.exception-type &= attribute id { xsd:ID }?
attlist.location &= attribute id { xsd:ID }?
attlist.resource-ref &= attribute id { xsd:ID }?
attlist.res-ref-name &= attribute id { xsd:ID }?
attlist.res-type &= attribute id { xsd:ID }?
attlist.res-auth &= attribute id { xsd:ID }?
attlist.security-constraint &= attribute id { xsd:ID }?
attlist.web-resource-collection &= attribute id { xsd:ID }?
attlist.web-resource-name &= attribute id { xsd:ID }?
attlist.http-method &= attribute id { xsd:ID }?
attlist.user-data-constraint &= attribute id { xsd:ID }?
attlist.transport-guarantee &= attribute id { xsd:ID }?
attlist.auth-constraint &= attribute id { xsd:ID }?
attlist.role-name &= attribute id { xsd:ID }?
attlist.login-config &= attribute id { xsd:ID }?
attlist.realm-name &= attribute id { xsd:ID }?
attlist.form-login-config &= attribute id { xsd:ID }?
attlist.form-login-page &= attribute id { xsd:ID }?
attlist.form-error-page &= attribute id { xsd:ID }?
attlist.auth-method &= attribute id { xsd:ID }?
attlist.security-role &= attribute id { xsd:ID }?
attlist.security-role-ref &= attribute id { xsd:ID }?
attlist.role-link &= attribute id { xsd:ID }?
attlist.env-entry &= attribute id { xsd:ID }?
attlist.env-entry-name &= attribute id { xsd:ID }?
attlist.env-entry-value &= attribute id { xsd:ID }?
attlist.env-entry-type &= attribute id { xsd:ID }?
attlist.ejb-ref &= attribute id { xsd:ID }?
attlist.ejb-ref-name &= attribute id { xsd:ID }?
attlist.ejb-ref-type &= attribute id { xsd:ID }?
attlist.home &= attribute id { xsd:ID }?
attlist.remote &= attribute id { xsd:ID }?
attlist.ejb-link &= attribute id { xsd:ID }?
start = web-app
