;;; sql-config.el --- configure SQL modes

;;; Commentary:

;;; Code:

;; Oracle SQL stuff
(autoload 'sql-oracle "sql" "Interactive SQL mode." t)

(provide 'sql-config)
;;; sql-config.el ends here
