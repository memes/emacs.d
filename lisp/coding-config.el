;;; coding-config.el --- Configuration for coding

;;; Commentary:

;;; Code:

(defvar memes-packages)

;; Show matching parenthesis where possible
(show-paren-mode 1)

;; Enable paredit mode
(add-to-list 'memes-packages 'paredit)
;; List of hooks to add paredit support
;;  - add to this list when modes should hook paredit
(defvar memes-paredit-mode-hooks
  '(emacs-lisp-mode-hook lisp-mode-hook)
  "Hooks to include paredit support.")

;; Enable paredit for list like modes
(with-eval-after-load "paredit"
  ;; Add paredit mode to all hooks in memes-paredit-mode-hooks
  (mapc (lambda (hook)
	  (add-hook hook (lambda ()
			   (paredit-mode +1))))
	memes-paredit-mode-hooks))

;; Patch/diff mode if installed on OS
(cond ((fboundp 'diff-mode)
       ;; Autoload diff mode for diff/patch like files
       (autoload 'diff-mode "diff-mode" "Diff major mode" t)
       (setq diff-switches "-Naur")
       (add-to-list 'auto-mode-alist '("\\.\\(diffs?\\|patch\\|rej\\)\\'" . diff-mode))))

;; Add flycheck to all supported languages
(add-to-list 'memes-packages 'flycheck)

;; Configure flycheck after initialisation is compelete
(defun memes-init-flycheck ()
  "Turn on flycheck everywhere."
  (require 'flycheck)
  (global-flycheck-mode)
  ;; Disable jshint and json checkers
  (setq-default flycheck-disabled-checkers
		(append flycheck-disabled-checkers '(javascript-jshint json-jsonlint)))
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (flycheck-add-mode 'javascript-eslint 'js-mode))
(add-hook 'memes-after-load-packages-hook 'memes-init-flycheck)

;; Set C-x c to launch compile command
(setq-default compilation-read-command nil)
(global-set-key "\C-xc" 'compile)

;; C-mode hook common to all sub-modes
(add-to-list 'memes-packages 'ggtags)
(defun memes-c-mode-common-hook ()
  "Hook common to all 'c-mode' derived modes."
  (setq-default c-basic-offset 4)
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'statement-case-open 0)
  (c-set-offset 'case-label '+)
  (setq-default tab-width 4 indent-tabs-mode nil)
  (turn-on-auto-fill)
  (flyspell-prog-mode)
  (ggtags-mode 1))
(add-hook 'c-mode-common-hook 'memes-c-mode-common-hook)

;; Define a set of path/c-style option pairs
(defvar memes-c-style-alist
      '(("~/projects/kernel" . "linux-tabs-only")
	(nil . "k&r"))
      "List of paths and c-style options to apply.")

;; Function to pick a c-style based on filepath
(defun memes-choose-c-style ()
  "Choose a C-style based on filepath of buffer."
  (let ((style
	 (assoc-default buffer-file-name memes-c-style-alist
			(lambda (pattern path)
			  (or (not pattern)
			      (and (stringp path)
				   (string-match (expand-file-name pattern)
						 path))))
			;; Add a default in case there are no matches
			'(nil . "k&r"))))
    (cond
     ((stringp style) (c-set-style style))
     ((functionp style) (style)))))
(add-hook 'c-mode-hook 'memes-choose-c-style)

;; Setup Linux style - mostly from Documentation/CodingStyle
(defun c-lineup-arglist-tabs-only (ignored)
  "Line up argument lists by tabs, not spaces."
  (let* ((anchor (c-langelem-pos c-syntactic-element))
	 (column (c-langelem-2nd-pos c-syntactic-element))
	 (offset (- (1+ column) anchor))
	 (steps (floor offset c-basic-offset)))
    (* (max steps 1)
       c-basic-offset)))
(defconst linux-tabs-only
  '("linux"
    (c-offsets-alist
     (arglist-cont-nonempty
      c-lineup-gcc-asm-reg
      c-lineup-arglist-tabs-only)))
  "Linux CodingStyle recommendations.")
(c-add-style "linux-tabs-only" linux-tabs-only)

;; Groovy/Grails integration
(add-to-list 'memes-packages 'groovy-mode)
(with-eval-after-load "groovy-mode"
  (autoload 'groovy-mode "groovy-mode" "Major mode for editing Groovy code." t)
  (add-to-list 'auto-mode-alist '("\.groovy$" . groovy-mode))
  (add-to-list 'interpreter-mode-alist '("groovy" . groovy-mode))
  (add-hook 'groovy-mode-hook
	    '(lambda ()
	       (require 'groovy-electric)
	       (groovy-electric-mode))))

;; Lua mode
(add-to-list 'memes-packages 'lua-mode)
(with-eval-after-load 'lua-mode
  (autoload 'lua-mode "lua-mode" "Lua editing mode" t)
  (add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
  (add-to-list 'interpreter-mode-alist '("lua" . lua-mode)))

;; Use Octave for .m files in preference to MATLAB when OS package is installed
(cond ((fboundp 'octave-mode)
       (setq-default octave-block-offset 4)
       (setq auto-mode-alist (cons '("\\.m\\'" . octave-mode) auto-mode-alist))))

;; Load scala from package
(add-to-list 'memes-packages 'scala-mode)

;; Clojure package
(add-to-list 'memes-packages 'clojure-mode)
(with-eval-after-load "clojure-mode"
  (add-to-list 'memes-paredit-mode-hooks 'clojure-mode-hook))
(add-to-list 'memes-packages 'cider)
(with-eval-after-load "cider-mode"
  (add-hook 'cider-mode-hook #'eldoc-mode))

;; Go language support
(add-to-list 'memes-packages 'go-mode)
(add-to-list 'memes-packages 'go-eldoc)
(add-to-list 'memes-packages 'go-errcheck)
(add-to-list 'memes-packages 'go-rename)
(add-to-list 'memes-packages 'go-guru)
(add-to-list 'memes-packages 'company-go)
(add-to-list 'memes-packages 'go-dlv)
(add-to-list 'memes-packages 'go-add-tags)
(add-to-list 'memes-packages 'flycheck-gometalinter)
(defconst memes-goroot
  (convert-standard-filename (expand-file-name
			      (cond ((string-equal system-type "windows-nt") "~/lib/go")
				    ((string-equal system-type "darwin") "~/Library/go")
				    (t "~/lib/go"))))
  "Local GOROOT customisations based on OS.")
(defun memes-gb-project-path (filename)
  "Return the gb project path for FILENAME, or nil if not using gb."
  (let ((gb-info-results (shell-command-to-string (format "cd %s && gb info" (directory-file-name (file-name-directory filename))))))
    (if (string-match "GB_PROJECT_DIR=\"\\(.*\\)\"" gb-info-results)
	(let ((gb-project-path (match-string 1 gb-info-results)))
	  (if (and (file-directory-p (concat gb-project-path "/src"))
		   (file-directory-p (concat gb-project-path "/vendor")))
	      gb-project-path
	    nil))
      nil)))
(defun memes-go-compile ()
  "Return a string of shell commands to compile current project."
  (let ((gb-project-path (memes-gb-project-path buffer-file-name)))
    (if gb-project-path
	(format "cd %s && gb build && gb test -v=1 && go tool vet %s/src" gb-project-path gb-project-path)
      (let ((go-project-path (or (directory-file-name (file-name-directory (memes-find-parent "src" buffer-file-name)))
				 (directory-file-name (file-name-directory buffer-file-name)))))
	(format "cd %s && go test -v $(go list ./... | grep -v /vendor/ | grep -v '^_') && go install $(go list ./... | grep -v /vendor/ | grep -v '^_')" go-project-path)))))
(defun memes-go-mode-hook ()
  "Hook to be executed in all go buffers."
  (setq-default gofmt-command "goimports"
		go-add-tags-style 'lower-camel-case
		flycheck-gometalinter-vendor t)
  (flycheck-gometalinter-setup)
  (go-eldoc-setup)
  (set (make-local-variable 'company-backends) '(company-go))
  (company-mode +1)
  (ggtags-mode +1)
  (make-local-variable 'process-environment)
  (setenv "GOPATH"
	  (mapconcat 'identity
		     (append (split-string (or (go-guess-gopath) "") path-separator) (list memes-goroot))
		     path-separator))
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "C-c C-r") 'go-remove-unused-imports)
  (local-set-key (kbd "C-c i") 'go-goto-imports)
  (local-set-key (kbd "C-c r") 'go-rename)
  (local-set-key (kbd "C-c t") 'go-add-tags)
  (add-hook 'before-save-hook 'gofmt-before-save)
  (set (make-local-variable 'compile-command) (memes-go-compile))
  (go-guru-hl-identifier-mode)
  )
(add-hook 'go-mode-hook 'memes-go-mode-hook)

;; Javascript support
(add-to-list 'memes-packages 'js2-mode)
(add-to-list 'memes-packages 'ac-js2)
(with-eval-after-load 'ac-js2
  (add-to-list 'company-backends 'ac-js2-company))
(defun memes-js-mode-hook ()
  "Hook to be executed for js modes."
  (js2-minor-mode)
  (ac-js2-mode)
  (setq-default js-indent-level 2
		js2-basic-offset 2
		js2-bounce-indent-p t
		indent-tabs-mode nil))
(add-hook 'js-mode-hook 'memes-js-mode-hook)
(add-to-list 'interpreter-mode-alist '("node" . js2-mode))

;; Stolen from http://stackoverflow.com/a/35806330 - better JSON support in js2-mode
(make-variable-buffer-local 'js2-parse-as-json)
(defadvice js2-reparse (before json)
  (setq js2-buffer-file-name buffer-file-name))
(ad-activate 'js2-reparse)
(defadvice js2-parse-statement (around json)
  (if (and (= tt js2-LC)
	   js2-buffer-file-name
	   (or js2-parse-as-json
	       (string-equal (substring js2-buffer-file-name -5) ".json"))
	   (eq (+ (save-excursion
		    (goto-char (point-min))
		    (back-to-indentation)
		    (while (eolp)
		      (next-line)
		      (back-to-indentation))
		    (point)) 1) js2-ts-cursor))
      (setq ad-return-value (js2-parse-assign-expr))
    ad-do-it))
(ad-activate 'js2-parse-statement)
(define-derived-mode json-mode js2-mode "JSON"
  "Major mode for editing JSON data."
  :group 'json
  (setq js2-parse-as-json t)
  (js2-reparse t))
(add-to-list 'auto-mode-alist '("\\.json$" . json-mode))

;; Coffee script support
(add-to-list 'memes-packages 'coffee-mode)
(defun memes-coffee-mode-hook ()
  "Hook to be executed for coffee-mode."
  (coffee-tab-width 2))
(add-hook 'coffee-mode-hook 'memes-coffee-mode-hook)

;; TypeScript support
(add-to-list 'memes-packages 'tide)
(defun memes-tide-mode-hook ()
  "Hook executed for typescript-mode."
  (tide-setup)
  (flycheck-mode +1)
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (company-mode +1)
  (add-hook 'before-save-hook 'tide-format-before-save nil t))
(add-hook 'typescript-mode-hook #'memes-tide-mode-hook)
(setq-default typescript-indent-level 2
	      tide-format-options
	      '(:tabSize 2 
			 :indentSize 2
			 :insertSpaceAfterCommaDelimiter t
			 :insertSpaceAfterSemicolonInForStatements t
			 :insertSpaceBeforeAndAfterBinaryOperators t
			 :insertSpaceAfterKeywordsInControlFlowStatements t
			 :insertSpaceAfterFunctionKeywordForAnonymousFunctions t
			 :insertSpaceAfterOpeningAndBeforeClosingNonemptyParenthesis nil
			 :insertSpaceAfterOpeningAndBeforeClosingNonemptyBrackets nil
			 :insertSpaceAfterOpeningAndBeforeClosingTemplateStringBraces nil
			 :insertSpaceAfterOpeningAndBeforeClosingJsxExpressionBraces nil
			 :placeOpenBraceOnNewLineForControlBlocks nil
			 :placeOpenBraceOnNewLineForFunctions nil))

;; C# mode
(add-to-list 'memes-packages 'csharp-mode)
(add-to-list 'memes-packages 'omnisharp)
(defun memes-csharp-mode-hook ()
  "C# mode hook."
  (electric-pair-mode 1)
  (omnisharp-mode))
(add-hook 'csharp-mode-hook 'memes-csharp-mode-hook)
(autoload 'csharp-mode "csharp-mode" "Major mode for editing C# code." t)
(setq auto-mode-alist (append '(("\\.cs$" . csharp-mode)) auto-mode-alist))

;; Swift support
(add-to-list 'memes-packages 'swift-mode)
(defun memes-swift-mode-hook ()
  "Swift mode hook."
  (add-to-list 'flycheck-checkers 'swift)
  (setq flycheck-swift-target nil
	swift-repl-executable (cond ((string-equal system-type "darwin") "xcrun swift")
				    (t "swift"))))
(add-hook 'swift-mode-hook 'memes-swift-mode-hook)

;; Protobuf support
(add-to-list 'memes-packages 'protobuf-mode)
(defun memes-protobuf-mode-hook ()
  "Protobuf mode hook."
  (add-to-list 'flycheck-checkers 'protobuf-protoc-reporter t))
(add-hook 'protobuf-mode-hook 'memes-protobuf-mode-hook)

;; Angular 2 support
(add-to-list 'memes-packages 'ng2-mode)
(add-hook 'ng2-mode-hook 'memes-tide-mode-hook)
(defun memes-ng2-html-mode-hook ()
  "Hook for ng2 HTML mode."
  (auto-fill-mode 0))
(add-hook 'ng2-html-mode-hook 'memes-ng2-html-mode-hook)

;; elixir
(add-to-list 'memes-packages 'elixir-mode)
(add-to-list 'memes-packages 'alchemist)
(add-to-list 'memes-packages 'ruby-end)
(defun memes-elixir-mode-hook ()
  "Elixir mode hook."
  (alchemist-mode)
  (company-mode +1)
  (ruby-end-mode))
(with-eval-after-load 'elixir-mode
  (add-hook 'elixir-mode-hook 'memes-elixir-mode-hook))

;; Doing java again, this time with eclim
(add-to-list 'memes-packages 'eclim)
(add-to-list 'memes-packages 'company-emacs-eclim)
(add-to-list 'memes-packages 'gradle-mode)
(with-eval-after-load 'eclim
  (setq-default eclim-executable "~/eclipse/eclim"
                eclimd-autostart t)
  (company-emacs-eclim-setup))
(defun memes-java-mode-hook ()
  "Java mode hook."
  (eclim-mode t)
  (gradle-mode t))
(add-hook 'java-mode-hook 'memes-java-mode-hook)

;; Make python support better
(add-to-list 'memes-packages 'elpy)
(add-to-list 'memes-packages 'py-autopep8)
(with-eval-after-load 'elpy
  (cond
   ((executable-find "python3")
    (setq elpy-rpc-python-command "python3"))
   ((executable-find "python2")
    (setq elpy-rpc-python-command "python2"))
   (t
    ()))
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))
(elpy-enable)
(defun memes-python-mode-hook ()
  "Python mode hook."
  (flycheck-mode +1)
  (py-autopep8-enable-on-save))
(add-hook 'elpy-mode-hook 'memes-python-mode-hook)

;; Dart support
(add-to-list 'memes-packages 'dart-mode)
(setq-default dart-enable-analysis-server t
              dart-format-on-save t)
(defun memes-dart-mode-hook ()
  "Dart mode hook."
  (flycheck-mode +1))
(add-hook 'dart-mode-hook 'memes-dart-mode-hook)

(provide 'coding-config)
;;; coding-config.el ends here
